import React from "react";
import "./App.css";
import CreateEventForm from "./CreateEventForm";

const App = () => {
  return (
    <div className="app">
      <h1 className="title">Events</h1>
      <h2>Add an event</h2>
      <CreateEventForm />
    </div>
  );
};

export default App;
