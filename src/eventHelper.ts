// Helper variables to try and limit as much duplicate code as possible e.g. in case
// back-end names change etc.

export const NAME = "name";
export const DESCRIPTION = "description";
export const START_DATE = "dateStart";
export const END_DATE = "dateEnd";

export interface IEvent {
  [NAME]: string;
  [DESCRIPTION]: string;
  [START_DATE]: string;
  [END_DATE]: string;
}

export const dateFormat = "YYYY-MM-DD";
