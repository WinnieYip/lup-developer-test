import moment from "moment";
import React, { FormEvent, useState } from "react";
import "./CreateEventForm.css";
import {
  dateFormat,
  DESCRIPTION,
  END_DATE,
  IEvent,
  NAME,
  START_DATE,
} from "./eventHelper";
import { toast } from "react-toastify";

const CreateEventForm = () => {
  const defaultDate = moment(new Date()).format(dateFormat);
  const [event, setEvent] = useState({
    [NAME]: "",
    [DESCRIPTION]: "",
    [START_DATE]: defaultDate,
    [END_DATE]: defaultDate,
  });

  // Modify the state whenever any input to the form has changed
  const handleFormChange = (name: keyof IEvent, value: string) => {
    setEvent({ ...event, [name]: value });
  };

  const convertToUTC = () => {
    setEvent({
      ...event,
      [START_DATE]: moment.utc(event[START_DATE]).format(dateFormat),
      [END_DATE]: moment.utc(event[END_DATE]).format(dateFormat),
    });
  };

  const postEvent = () => {
    convertToUTC();
    console.log("Submitting...");
    fetch("https://localhost:5001/events", {
      method: "post",
      body: JSON.stringify(event),
    }).then((response) => {
      toast.dark(response.text()); // Show response in toast notification....
      return response.json();
    });
  };

  const handleFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    postEvent();
  };

  return (
    <div className="create-event-form">
      <form onSubmit={(e) => handleFormSubmit(e)}>
        <label>
          <b>Event name</b>
        </label>
        <div>
          <input
            required
            type="text"
            name={NAME}
            value={event[NAME]}
            onChange={(e) => handleFormChange(NAME, e.target.value)}
          />
        </div>
        <label>
          <b>Description</b>
        </label>
        <div className="description">
          <textarea
            value={event[DESCRIPTION]}
            onChange={(e) => handleFormChange(DESCRIPTION, e.target.value)}
            required
            rows={5}
            cols={22}
          />
        </div>
        <label>
          <b>Start date</b>
        </label>
        <div>
          <input
            required
            type="date"
            name={START_DATE}
            value={event[START_DATE]}
            onChange={(e) => handleFormChange(START_DATE, e.target.value)}
            min="2000-01-01"
            max={event[END_DATE]}
          />
        </div>
        <label>
          <b>End date</b>
        </label>
        <div>
          <input
            required
            type="date"
            name={END_DATE}
            value={event[END_DATE]}
            onChange={(e) => handleFormChange(END_DATE, e.target.value)}
            min={event[START_DATE]}
            max="2050-12-31"
          />
        </div>
        <input type="submit" value="Submit event" />
      </form>
    </div>
  );
};

export default CreateEventForm;
